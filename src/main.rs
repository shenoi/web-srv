#[async_std::main]
async fn main() -> tide::Result<()> {
    let mut app = tide::new();
    app.at("/").get(|_| async { Ok("Hello, world!") });
    app.at("/examples").serve_dir("examples/")?;
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}